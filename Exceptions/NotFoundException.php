<?php

//Excepciones para errores en la busqueda de registros

class NotFoundException extends Exception

{

    public function __construct (string $message) {

        parent::__construct($message);

    }

}
?>