<?php
//Creacion de otro array de errores para las excepciones
$errores = [];

//Creacion del objeto usuarioRepository para poder usar el querybuilder
$usuarioRepository = new UsuarioRepository();
try{

    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        //Creacion de la variable usuario que contendra el objeto que coja del findByName
        $usuario = $usuarioRepository->findByName($_POST["login_name"]);
    
        //Esto errores son del inteliphense solo no tiene que ver con el codigo
        //Aqui cojemos el nombre y la contraseña y las almacenamos en variables 
        $nombre = $usuario->getNombre();
        $pass = $usuario->getPassword();
        //Aqui hacemos la comparacion de las variables con lo que se puso en el login 
        if ($_POST["login_name"] == $nombre && $_POST["login_pass"] == $pass) {
            //Y creaamos la session 
            $_SESSION["account"] = $nombre;
            //para luego redirigirnos al index
            header("Location: index");
            
        }
    }
}catch (NotFoundException $notFoundException){
    $errores[] = $notFoundException->getMessage();
}





//Vista del login 
require __DIR__ . "/../views/login.view.php";
