<?php
//LLamamiento a el fichero bootstrap donde tenemos todos los requires
require_once "core/bootstrap.php";

//Creacion de un tryCatch para hacer el findAll de las tareas y si no hay ningun registro que capturemos la excepcion.
try{
    $taskRepository = new TaskRepository();
    $tareas = $taskRepository->findAll();
} catch (NotFoundException $notFoundException){
    $errores[] = $notFoundException->getMessage();
}

// Vista del index
if(!isset($_SESSION["account"])){
    header("Location: login");
}

require __DIR__ . "/../views/index.view.php";
?>

