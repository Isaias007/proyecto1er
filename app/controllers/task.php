<?php
//Creacion de un array de errores donde se pondran las excepciones y los errores de la validacion
$errores = [];


try {
    
    // Conseguimos del contenedor la conexion con un getter
    $connection = App::getConnection();

    //Instanciamos los idiomas y las tareas con su repositorio
    $idiomaRepository = new IdiomaRepository();
    $taskRepository = new TaskRepository();

    //Validamos si existe el metodo post y es cuando hacemos las validaciones de nombres, correo , pagos etc...
    if ($_SERVER["REQUEST_METHOD"] === "POST") {

        if (
            empty($_POST["nombre"]) && empty($_POST["idiomas"]) && empty($_POST["email"]) && empty($_POST["telefono"]) && empty($_POST["requerimientos"])
            && empty($_POST["pago"])
        ) {
            array_push($errores, "No hay ningun parametro en el formulario");
        } else if (empty($_POST["nombre"])) {
            array_push($errores, "No hay ningun parametro en nombre");
        } else if (empty($_POST["requerimientos"])) {
            array_push($errores, "No hay ningun objetivo en la tarea");
        } else if (empty($_POST["email"])) {
            array_push($errores, "No hay ningun correo en la tarea");
        } else if ((strpos($_POST["email"], "@") == false)) {
            array_push($errores, "El correo esta mal escrito");
        } else if (empty($_POST["pago"])) {
            array_push($errores, "No hay pago para este trabajo");
        } else {

            //Aqui evitamos la inyeccion de html y con los repositorios que usan la clase abstracta QueryBuilder evitamos la inyeccion SQL
            $nombre = trim(htmlspecialchars($_POST["nombre"]));
            $idioma = trim(htmlspecialchars($_POST["idioma"]));
            $requerimientos = trim(htmlspecialchars($_POST["requerimientos"]));
            $telefonos = trim(htmlspecialchars($_POST["telefono"]));
            $email = $_POST["email"];
            $pago = trim(htmlspecialchars($_POST["pago"]));

            //Instanciamos un nuevo objeto tarea
            $task = new Task($nombre, $email, $idioma, $telefonos, $requerimientos, $pago);

            //Con la funcion Save de el QueryBuilder guardamos el objeto en la base de datos 
            $taskRepository->save($task);

            //Creamos el mensaje de respuesta
            $mensaje = "Todo se a enviado correctamente :3";

            App::get("logger")->add($mensaje);

            //Y redireccionamos a la pagina principal donde se mostraran las tareas
            header("Location: index");
        }
    }

    //Este findAll de los idiomas lo utilizaremos para el select de la view que nos mostrara los idiomas a elegir
    $idiomas = $idiomaRepository->findAll();
    



    //Capturamos las excepciones
} catch (FileException $fileException) {

    $errores[] = $fileException->getMessage();
} catch (AppException $appException) {

    $errores[] = $appException->getMessage();
} catch (NotFoundException $notFoundException){

}

//Vista de el formulario de creacion de tareas

if(!isset($_SESSION["account"])){
    header("Location: login");
}

require __DIR__ . "/../views/task.view.php";
