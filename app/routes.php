<?php 
//Fichero donde se guardan las rutas de la pagina 
return [

 "PHP/dsw/proyecto1er/proyecto1er/index" => "app/controllers/index.php",

 "PHP/dsw/proyecto1er/proyecto1er/about" => "app/controllers/about.php",

 "PHP/dsw/proyecto1er/proyecto1er/cource" => "app/controllers/cource.php",

 "PHP/dsw/proyecto1er/proyecto1er/blog" => "app/controllers/blog.php",

 "PHP/dsw/proyecto1er/proyecto1er/single-blog" => "app/controllers/single-blog.php",

 "PHP/dsw/proyecto1er/proyecto1er/elements" => "app/controllers/elements.php",

 "PHP/dsw/proyecto1er/proyecto1er/contact" => "app/controllers/contact.php",

 "PHP/dsw/proyecto1er/proyecto1er/task" => "app/controllers/task.php",

 "PHP/dsw/proyecto1er/proyecto1er/login" => "app/controllers/login.php",

 "PHP/dsw/proyecto1er/proyecto1er/logOut" => "app/controllers/logOut.php",
]

?>