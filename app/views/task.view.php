<!-- Partial Head -->

<?php include __DIR__ . "/partials/head-doc.part.php"; ?>

<body>
	<!-- Partial nav -->

	<?php include __DIR__ . "/partials/nav-doc.part.php"; ?>

	<!-- breadcrumb start-->
	<section class="breadcrumb breadcrumb_bg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="breadcrumb_iner text-center">
						<div class="breadcrumb_iner_item">
							<h2>Trabajos</h2>
							<p>Home <span>/</span>Trabajos</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<div class="section-top-border">
		<div class="row justify-content-center">
			<div class="col-lg-8 col-md-8">
				<h3 class="mb-30 text-center">Formulario de Trabajo</h3>
				<form action="#" method="POST">
					<div class="mt-10">
						<input type="text" name="nombre" placeholder="Nombre de la tarea" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nombre'" class="single-input" value="<?= @$_POST["nombre"] ?>">
					</div>
					<div class="mt-10">
						<input type="email" name="email" placeholder="Correo Electronico" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Correo Electronico'" class="single-input" value="<?= @$_POST["email"] ?>">
					</div>
					<div class="mt-10">
					<select class="mt-10" name="idioma">
                        <?php foreach ($idiomas as $idiom) : ?>
                            <option value="<?= $idiom->getId() ?>"><?= $idiom->getNombre(); ?></option>
                        <?php endforeach; ?>
                    </select>
					</div>					
					<div class="mt-10">
						<input type="text" name="telefono" placeholder="Número de Telefono" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Número de Telefono'" maxlength="9" class="single-input" value="<?= @$_POST["telefono"] ?>">
					</div>
					<div class="mt-10">
						<textarea class="single-textarea" name="requerimientos" placeholder="Requerimientos" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Requerimientos'"><?php echo @$_POST["requerimientos"] ?></textarea>
					</div>
					<div class="mt-10">
						<input type="text" name="pago" placeholder="Pago" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Pago'" class="single-input" value="<?= @$_POST["pago"] ?>">
					</div>
					<div class="mt-10">
						<button type="submit" class="btn_1">Crear</button>
					</div>
				</form>
				<hr>
				<div class="mt-10 text-center">
					<?php if (empty($errores) == false) : ?>
						<div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
							<button type="button" class="clase" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">x</span>
							</button>
							<ul>
								<?php foreach ($errores as $error) : ?>
									<li><?= $error ?></li>
								<?php endforeach; ?>
							</ul>
						</div>
					<?php else : ?>
						<p><?= isset($mensaje) ? $mensaje : "" ?></p>
					<?php
					endif;
					?>
				</div>
			</div>
		</div>
	</div>

	<!-- Partial Footer -->

	<?php include __DIR__ . "/partials/footer-doc.part.php"; ?>