<!-- Partial Head -->
<?php include __DIR__ . "/partials/head-doc.part.php"; ?>

<body>
    <!-- Partial nav -->

    <?php include __DIR__ . "/partials/nav-doc.part.php"; ?>


    <!-- banner part start-->
    <section class="banner_part">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-xl-6">
                    <div class="banner_text">
                        <div class="banner_text_iner">
                            <h5>ProyectArte360</h5>
                            <h1>Miles de Freelances dispuestos a ayudarte</h1>
                            <p>Muchas tareas creadas para que les den una respuesta todo con un respectivo pago</p>
                            <a href="#Trabajos" class="btn_1">Mirar Trabajos </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner part start-->

    <!-- feature_part start-->
    <section class="feature_part mb_110">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xl-3 align-self-center">
                    <div class="single_feature_text ">
                        <h2>Crea tu <br> Trabajo</h2>
                        <p>Cuéntanos en simples pasos qué proyecto quieres realizar. ¡Gratis y sin compromiso!</p>
                        <a href="#" class="btn_1">Read More</a>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3">
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <span class="single_feature_icon"><i class="ti-layers"></i></span>
                            <h4>Seleccionar</h4>
                            <p>Recibe propuestas de excelentes freelancers. Elige al mejor para tu proyecto.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3">
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <span class="single_feature_icon"><i class="ti-new-window"></i></span>
                            <h4>Empieza ahora</h4>
                            <p>Realizas el pago con garantía total sobre los fondos y empieza a trabajar :3</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3">
                    <div class="single_feature">
                        <div class="single_feature_part single_feature_part_2">
                            <span class="single_service_icon style_icon"><i class="ti-light-bulb"></i></span>
                            <h4>Aceptar</h4>
                            <p>Recibe el proyecto terminado y libera los fondos al freelancer.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- upcoming_event part start-->

    <!-- member_counter counter start -->
    <section class="member_counter">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">1024</span>
                        <h4>Tareas Realizadas</h4>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">960</span>
                        <h4>Freelances en movimiento</h4>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">1018</span>
                        <h4>Creadores contentos</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- member_counter counter end -->

    <!--::blog_part start::-->
    <section id="Trabajos" class="blog_part section_padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5">
                    <div  class="section_tittle text-center">
                        <h2>Trabajos por hacer</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <?php
                if (empty($tareas) == false) {


                    foreach ($tareas as $tarea) {



                ?>
                        <div class="card" style="width:700px; margin-bottom: 60px; border: 2px solid black;">
                        
                            <div class="card-body">
                                <h2 class="card-title"><?=$tarea->getNombre()?></h2>
                                <h5 class=""><strong>Correo:</strong> <?=$tarea->getEmail()?></h5>
                                <h5 class=""><strong>Telefono:</strong> <?=$tarea->getTelefono()?></h5>
                                <h5 class=""><strong>Idioma:</strong> <?= $taskRepository->getIdioma($tarea)->getNombre() ?></td></h5>
                                <p class="card-text"><?= $tarea->getRequerimientos()  ?></p>
                                <h5 class="text-right"><strong>Pago:</strong> <?=$tarea->getPago()?> €</h5>
                            </div>
                            <button class="btn_3">Enviar Propuesta</button>
                        </div>
                <?php
                    }
                }
                ?>
            </div>
        </div>
    </section>
    <!--::blog_part end::-->

    <!-- Partial Footer -->

    <?php include __DIR__ . "/partials/footer-doc.part.php"; ?>