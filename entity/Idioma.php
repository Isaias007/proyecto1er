<?php


//Clase Idioma con propiedades id y nombre que la usaremos en el formulario 

class Idioma implements IEntity
{

    private $id;

    private $nombre;

    public function __construct($id = 0, $nombre = "")

    {

        $this->id = $id;

        $this->nombre = $nombre;

    }


// Sus respectivos getters

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of nombre
     */
    public function getNombre()
    {
        return $this->nombre;
    }



// Y la funcion toArray que mete sus propiedades en un array
    public function toArray(): array
    {
        return [
            "id" => $this->getId(),

            "nombre" => $this->getNombre()
        ];
    }
}