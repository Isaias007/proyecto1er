<?php


class TaskRepository extends QueryBuilder

{

    public function __construct(string $table = "tareas", string $classEntity = "Task")

    {

        parent::__construct($table, $classEntity);
    }

    public function getIdioma(Task $task): Idioma

    {

        $idiomaRepository = new IdiomaRepository();

        return $idiomaRepository->find($task->getIdioma());
    }

}

?>